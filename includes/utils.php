<?php
/**
 * Created by PhpStorm.
 * User: imac1
 * Date: 22/03/2017
 * Time: 17:16
 */

if( !defined( 'ABSPATH' ) )
    exit;

class BD_Util {
    public static function getCategories() {

        $category_args = array(
            'taxonomy' => 'product_cat',
            'parent' => 0,
            'hierarchical' => 1,
            'hide_empty' => 0,
            'children' => false
        );
        $categories = get_categories($category_args);
        foreach ($categories as &$category) {
            $category_args['parent'] = $category->term_id;
            $category->children = get_categories($category_args);
        }
        return $categories;

    }
    public static function categorySelect() {
        $categories = self::getCategories();

        $options = array();
        foreach ($categories as $category) {
            $options[$category->term_id] = $category->name;
            foreach ($category->children as $category) {
                $options[$category->term_id] = '- ' . $category->name;
            }
        }
        return $options;
    }

    public static function arrayToSelect($array, $selected = '')
    {
        $output = "";
        $create_labels = (array_keys($array)[0] === 0);
        foreach ($array as $value => $text) {
            $option = "";
            if ($create_labels) {
                $value = $text;
            }
            if (is_array($text)) {
                $option .= '<optgroup id="'.$value.'" label="'.$text['label'].'">';
                unset($text['label']);
                $option .= arrayToSelect($text, $selected); //YAY RECURSIVE!!
                $option .= '</optgroup>' . "\n";
                $output .= $option;
                continue;
            }

            $option .= '<option title="'.$text.'" value="'.$value.'" ';
            if ($value == $selected) {
                $option .= 'selected=selected';
            }
            $option .=  ' >'.$text.'</option>' . "\n";
            $output .= $option;
        }
        return ($output);
    }

    public static function get_cheapest_price($product_id)
    {

        $product = wc_get_product($product_id);
        $prices = array();

        try {
            while(!method_exists($product, 'get_available_variations')) {
                $product = $product->parent;
            }
            $available_variations = $product->get_available_variations();
        } catch(Exception $e) {
            die($e->getMessage());
        }

        foreach ($available_variations as $variant) {
            $prices[] = array(
                'period' => $variant['attributes']['attribute_pa_hire-duration'],
                'price' => $variant['display_regular_price']
            );
        }
        $cheapest = array('price' => 0);
        foreach ($prices as $item) {
            if ($item['price'] < $cheapest['price'] || $cheapest['price'] == 0) {
                $cheapest = $item;
            }
        }
        if ($cheapest['price'] <= 0) {
            return false;
        }

        $attribute_term = get_term_by('slug', $cheapest['period'], 'pa_hire-duration');

        return array('price' => $cheapest['price'], 'period' => $attribute_term->name);
    }

    public static function getCustomerData(WP_User $user)
    {
        global $wpdb;

        $table_name = BD_Customer_Pricing::get_instance()->table_name;
        $results = self::groupResults($wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE user_id = %d", $user->ID)));

        return $results;
    }

    public static function groupResults($results)
    {
        $formatted = array();
        foreach ($results as $result) {
            $result->discount = floatVal($result->discount);
            $formatted[$result->type][$result->id] = $result;
        }
        if(!empty($formatted['global']) && count($formatted['global']) <= 1) $formatted['global'] = array_shift($formatted['global']);

        if(!empty($formatted['product'])) {
            foreach ($formatted['product'] as &$row) {
                $product = wc_get_product($row->item_id);

                if (!$product) continue;

                $row->product_id = $row->item_id;
                $row->product_title = $product->get_formatted_name();
                if($product->get_parent()) {
                    $row->product_id = $product->get_parent();
                    $parent = wc_get_product($product->get_parent());
                    $row->product_title = $parent->get_formatted_name();
                    $price = BD_Util::get_cheapest_price($product->get_id());
                    $row->price = $price['price'] * (1 - ($row->discount / 100));
                } else {
                    $row->item_id = "*";
                }
            }
        }
        return $formatted;
    }

    public static function get_variations($product_id, $selected = '') {
        $product = wc_get_product($product_id);
        $options = array('*' => "All options");

        if ($product) {
            foreach ($product->get_children() as $child_id) {
                $child_product = wc_get_product($child_id);
                $options[$child_id] = $child_product->get_sku() . ' - ' . $child_product->post->post_content;
            }
        }
        return BD_Util::arrayToSelect($options, $selected);
    }


    public static function get_top_level_categories($product_id)
    {
        $product_terms = get_the_terms($product_id, 'product_cat');
        if(!$product_terms) return array();

        $top_categories = array();

        foreach($product_terms as $term) {
            $product_category =  $term->parent;
            $product_category_term = get_term($product_category, 'product_cat');
            $top_categories[$product_category_term->term_id] = $product_category_term;
            $product_category_parent = $product_category_term->parent;

            while ($product_category_parent  !=  0) {
                $product_category_term  =  get_term($product_category_parent, 'product_cat');
                $product_category_parent = $product_category_term->parent;
                $top_categories[$product_category_term->term_id] = $product_category_term;
            }
        }


        return $top_categories;
    }

    /**
     * Convert a multi-dimensional, associative array to CSV data
     * @param  array $data the array of data
     * @return string       CSV text
     */
    public static function str_putcsv($data) {
        # Generate CSV data from array
        $fh = fopen('php://temp', 'rw'); # don't create a file, attempt
        # to use memory instead

        # write out the headers
        fputcsv($fh, array_keys(current($data)));

        # write out the data
        foreach ($data as $row) {
            fputcsv($fh, $row);
        }
        rewind($fh);
        $csv = stream_get_contents($fh);
        fclose($fh);

        return $csv;
    }
}