<?php
$variation = wc_get_product($_GET['variation_id']);
$parent = wc_get_product($variation->get_parent());
$price = BD_Util::get_cheapest_price($variation->get_id());
?>
<form class="price-modal">
    <table class="form-table">
        <tr>
            <th><label>Item Name: </label></th>
            <td><?= $parent->get_title(); ?><br /><em>(<?= $variation->get_sku(); ?> - <?= $variation->post->post_content; ?>)</em></td>
        </tr>
        <tr>
            <th><label>List Price: </label></th>
            <td>
                <div class="input-group">
                    <span class="input-group-addon">&pound;</span>
                    <input type="number" readonly="readonly" class="original" value="<?= $price['price']; ?>" />
                </div>
                <p class="description"><?= $price['period']; ?></p>
            </td>
        </tr>
        <tr>
            <th><label>Desired Price: </label></th>
            <td>
                <div class="input-group">
                    <span class="input-group-addon">&pound;</span>
                    <input type="number" name="desired_price" class="desired" min="" max="" step="any" name="" value="" placeholder="" autofocus />
                </div>
            </td>
        </tr>
        <tr>
            <th><label>Price Difference: </label></th>
            <td>
                <div class="input-group">
                    <input type="number" readonly="readonly" class="difference" value="" />
                    <span class="input-group-addon">%</span>
                </div>
            </td>
        </tr>
    </table>
    <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Set Price"></p>
</form>