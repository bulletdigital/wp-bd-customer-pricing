<div class="wrap bd-wrap">
    <h1 class="wp-heading-inline">Customer Pricing: <?= get_user_meta($user->ID, 'billing_company', true) ? get_user_meta($user->ID, 'billing_company', true) : $user->user_nicename; ?></h1>
    <a href="<?= admin_url('users.php'); ?>" class="page-title-action">Return to Users</a>
    <hr class="wp-header-end" />
    <form method="post" action="<?= admin_url('admin.php'); ?>">

        <section>
            <h2>Global Discount</h2>
            <div class="input-group" style="max-width: 300px;">
                <input type="number" name="global[discount]" min="" max="" step="any" name="" value="<?= @$data['global']->discount; ?>" placeholder="" />
                <?php if(!empty($data['global'])): ?>
                <input type="hidden" name="global[id]" value="<?= @$data['global']->id; ?>" />
                <?php endif; ?>
                <span class="input-group-addon">%</span>
            </div>
            <p>&nbsp;</p>
        </section>

        <section>
            <h2>Category Discounts</h2>
            <table class="wp-list-table widefat striped bd-table" id="category-pricing">
                <thead>
                <tr>
                    <th scope="col" class="manage-column">Category</th>
                    <th scope="col" class="manage-column">Discount %</th>
                    <th scope="col" class="manage-column column-cb"></th>
                </tr>
                </thead>
                <tbody id="the-list">
                <?php if(!empty($data['category'])): ?>
                    <?php foreach($data['category'] as $row): ?>
                    <tr data-id="<?= $row->id; ?>">
                        <td width="50%">
                            <select name="category[item_id][]" class="wc-enhanced-select" data-placeholder="Select a category...">
                                <option></option>
                                <?= BD_Util::arrayToSelect(BD_Util::categorySelect(), $row->item_id); ?>
                            </select>
                        </td>
                        <td>
                            <div class="input-group">
                                <input type="number" name="category[discount][]" class="column_width" min="" max="" step="any" name="" value="<?= $row->discount; ?>" placeholder="" />
                                <span class="input-group-addon">%</span>
                            </div>
                        </td>
                        <td class="column-cb" valign="middle">
                            <a href="#" class="remove"><span class="dashicons dashicons-no-alt"></span></a>
                            <input type="hidden" name="category[id][]" value="<?= $row->id; ?>" />
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                <tr class="no-rows <?= (!empty($data['category'])) ? "hidden" : ""; ?>">
                    <td colspan="3">There are currently no category discounts.</td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <th scope="col" class="manage-column">Category</th>
                    <th scope="col" class="manage-column">Discount %</th>
                    <th scope="col" class="manage-column column-cb"></th>
                </tr>
                </tfoot>

            </table>
            <script class="blank-row" type="text/x-handlebars-template">
                <tr>
                    <td width="50%">
                        <select name="category[item_id][]" class="wc-enhanced-select" data-placeholder="Select a category...">
                            <option></option>
                            <?= BD_Util::arrayToSelect(BD_Util::categorySelect()); ?>
                        </select>
                    </td>
                    <td>
                        <div class="input-group">
                            <input type="number" name="category[discount][]" class="column_width" min="" max="" step="any" name="" value="" placeholder="" />
                            <span class="input-group-addon">%</span>
                        </div>
                    </td>
                    <td class="column-cb" valign="middle">
                        <a href="#" class="remove"><span class="dashicons dashicons-no-alt"></span></a>
                        <input type="hidden" name="category[id][]" value="0" />
                    </td>
                </tr>
            </script>
            <p class="submit"><button class="add-row button">Add Row</button></p>
            <p>&nbsp;</p>
        </section>

        <section>
            <h2>Product Discounts</h2>
            <table class="wp-list-table widefat striped bd-table" id="product-pricing">
                <thead>
                <tr>
                    <th scope="col" class="manage-column" colspan="2">Product</th>
                    <th scope="col" class="manage-column">Discount %</th>
                    <th scope="col" class="manage-column">Net Price</th>
                    <th scope="col" class="manage-column column-cb"></th>
                    <th scope="col" class="manage-column column-cb"></th>
                </tr>
                </thead>
                <tbody id="the-list">
                <?php if(!empty($data['product'])): ?>
                    <?php foreach($data['product'] as $row): ?>
                        <tr data-id="<?= $row->id; ?>">
                            <td width="30%">
                                <input type="hidden" name="product[product_id][]" class="wc-product-search" data-placeholder="Select a product..." value="<?= $row->product_id; ?>" data-selected="<?= $row->product_title; ?>" data-action="woocommerce_json_search_grouped_products" />
                            </td>
                            <td width="20%">
                                <select name="product[item_id][]" class="wc-enhanced-select variation-select" data-placeholder="Choose a product first...">
                                    <option></option>
                                    <?= BD_Util::get_variations($row->product_id, $row->item_id); ?>
                                </select>
                            </td>
                            <td>
                                <div class="input-group">
                                    <input type="number" name="product[discount][]" class="discount column_width" min="" max="" step="any" name="" value="<?= $row->discount; ?>" placeholder="" />
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="net-price">
                                <div class="input-group">
                                    <span class="input-group-addon">&pound;</span>
                                    <input type="text" class="column_width" readonly="readonly" value="<?= number_format(@$row->price, 2); ?>" placeholder="" />
                                </div>
                            </td>
                            <td class="column-cb" valign="middle">
                                <a href="#" class="calculate"><span class="dashicons dashicons-admin-generic"></span></a>
                            </td>
                            <td class="column-cb" valign="middle">
                                <a href="#" class="remove"><span class="dashicons dashicons-no-alt"></span></a>
                                <input type="hidden" name="product[id][]" value="<?= $row->id; ?>" />
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                <tr class="no-rows <?= (!empty($data['product'])) ? "hidden" : ""; ?>">
                    <td colspan="5">There are currently no product discounts.</td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <th scope="col" class="manage-column" colspan="2">Product</th>
                    <th scope="col" class="manage-column">Discount %</th>
                    <th scope="col" class="manage-column">Net Price</th>
                    <th scope="col" class="manage-column column-cb"></th>
                    <th scope="col" class="manage-column column-cb"></th>
                </tr>
                </tfoot>

            </table>
            <script class="blank-row" type="text/x-handlebars-template">
                <tr>
                    <td width="30%">
                        <input type="hidden" name="product[product_id][]" class="wc-product-search" data-placeholder="Select a product..." data-action="woocommerce_json_search_grouped_products" />
                    </td>
                    <td width="20%">
                        <select name="product[item_id][]" class="wc-enhanced-select variation-select" data-placeholder="Choose a product first...">
                            <option></option>
                        </select>
                    </td>
                    <td>
                        <div class="input-group">
                            <input type="number" name="product[discount][]" class="discount column_width" min="" max="" step="any" name="" value="" placeholder="" />
                            <span class="input-group-addon">%</span>
                        </div>
                    </td>
                    <td class="net-price">
                        <div class="input-group">
                            <span class="input-group-addon">&pound;</span>
                            <input type="text" class="column_width" readonly="readonly" value="" placeholder="" />
                        </div>
                    </td>
                    <td class="column-cb" valign="middle">
                        <a href="#" class="calculate"><span class="dashicons dashicons-admin-generic"></span></a>
                    </td>
                    <td class="column-cb" valign="middle">
                        <a href="#" class="remove"><span class="dashicons dashicons-no-alt"></span></a>
                        <input type="hidden" name="product[id][]" value="0" />
                    </td>
                </tr>
            </script>
            <p class="submit"><button class="add-row button">Add Row</button></p>
        </section>


        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button button-primary button-large" value="Save Pricing">
            <?php $nonce = wp_create_nonce( 'wp_rest' ); ?>
            <a href="<?= esc_url_raw( get_rest_url() ); ?>bd-customer-pricing/v1/customer/<?= $_REQUEST['user_id']; ?>/pricing.csv?_wpnonce=<?= $nonce; ?>" class="button button-large">Export Pricing Spreadsheet</a>
            <input type="hidden" name="action" value="bd_customer_pricing_save" />
            <input type="hidden" name="user_id" value="<?= $_REQUEST['user_id']; ?>" />
        </p>

    </form>
</div>