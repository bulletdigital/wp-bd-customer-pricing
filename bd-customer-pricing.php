<?php
/*
Plugin Name: BD Customer Pricing
Plugin URI: https://corecreative.co.uk
Description: Allows for customer specific pricing with extra features
Version: 0.1
Author: Oliver Sadler
Author Email: oliver@corecreative.co.uk
License:

  Copyright 2017 Oliver Sadler (oliver@bulletdigital.co.uk)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

if( !defined( 'ABSPATH' ) )
    exit;

require_once 'includes/utils.php';
class BD_Customer_Pricing {
    public $db_version = '1.0';

    private static $instance = null;
    public static function get_instance() {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function __construct()
    {
        global $wpdb;

        // add hooks
        $this->table_name = $wpdb->prefix . "bd_customer_pricing";
        $this->hooks();
    }

    private function hooks() {
        add_action('plugins_loaded', array( $this, 'db_update_check' ));
        add_action('admin_notices', array( $this, 'admin_notices' ));
        add_action('admin_menu', array( $this, 'admin_setup' ));
        add_action('rest_api_init', array( $this, 'register_routes' ));
        add_filter('user_row_actions', array( $this, 'user_actions' ), 10, 2);
        add_filter('woocommerce_get_price', array( $this, 'get_price' ), 50, 2);

        $action = 'bd_get_variations';
        add_action( 'wp_ajax_nopriv_' . $action, array( $this, 'get_variations' ));
        add_action( 'wp_ajax_' . $action, array( $this, 'get_variations' ));

        $action = 'bd_calculate_price';
        add_action( 'wp_ajax_' . $action, array( $this, 'price_modal' ));

        $action = 'bd_delete_row';
        add_action( 'wp_ajax_' . $action, array( $this, 'delete_row' ));

        $action = 'bd_customer_pricing_save';
        add_action( 'admin_action_' . $action, array( $this, 'handle_submit' ));

        register_activation_hook( __FILE__, array( $this, 'install' ));
    }

    public function install() {
        global $wpdb;

        $installed_ver = get_option( "bd_db_version" );

        if ( $installed_ver != $this->db_version ) {
            $charset_collate = $wpdb->get_charset_collate();

            $sql = "CREATE TABLE $this->table_name (
              id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
              user_id bigint(20) unsigned NOT NULL default '0',
              type varchar(255) default NULL,
              item_id bigint(20) unsigned NOT NULL default '0',
              discount decimal(20,16) NOT NULL default '0',
              PRIMARY KEY  (id),
              KEY user_id (user_id),
              KEY item_id (item_id)
            ) $charset_collate;";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);

            update_option("bd_db_version", $this->db_version);
        }
    }

    public function db_update_check() {
        if (get_site_option('bd_db_version') != $this->db_version) {
            $this->install();
        }
    }

    public function admin_notices() {
        $screen = get_current_screen();
        if ($screen->id == 'admin_page_customer-pricing') {

            if (isset($_GET['status'])) {

                if ($_GET['status'] == 'success') : ?>

                    <div class="notice notice-success is-dismissible">
                        <p>Customer bespoke prices updated successfully.</p>
                    </div>

                <?php else : ?>

                    <div class="notice notice-error is-dismissible">
                        <p><?php _e('No changes made.', 'bbb'); ?></p>
                    </div>

                <?php endif;

            }
        }
    }

    public function handle_submit() {
        global $wpdb;

        echo '<pre>';

        $data = array();

        if(!empty($_POST['global']) && !empty($_POST['global']['discount'])) {
            $row = array(
                'type' => 'global',
                'discount' => $_POST['global']['discount']
            );
            if($_POST['global']['id']) $row['id'] = $_POST['global']['id'];
            $data[] = $row;
        }

        foreach($_POST['category']['item_id'] as $i => $item_id) {
            $row = array(
                'type' => 'category',
                'item_id' => $item_id,
                'discount' => $_POST['category']['discount'][$i]
            );
            if($_POST['category']['id'][$i]) $row['id'] = $_POST['category']['id'][$i];
            $data[] = $row;
        }

        foreach($_POST['product']['item_id'] as $i => $item_id) {
            $row = array(
                'type' => 'product',
                'item_id' => $item_id,
                'discount' => $_POST['product']['discount'][$i]
            );
            if(!is_numeric($item_id)) {
                $row['item_id'] = $_POST['product']['product_id'][$i];
            }
            if($_POST['product']['id'][$i]) $row['id'] = $_POST['product']['id'][$i];
            $data[] = $row;
        }


        foreach($data as $row) {
            $row['user_id'] = intval($_POST['user_id']);
            if(isset($row['id']) && $row['id'] > 0) {
                $id = $row['id'];
                unset($row['id']);
                $wpdb->update($this->table_name, $row, array('id' => $id));
            } else {
                unset($row['id']);
                $wpdb->insert($this->table_name, $row);
            }
        }


        $redirect_url = add_query_arg(array('status' => 'success'), $_SERVER['HTTP_REFERER']);
        wp_redirect($redirect_url);
        exit();
    }

    public function assets() {
        $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
        wp_enqueue_style( 'bd_customer_pricing', plugins_url('assets/main.css', __FILE__) );
        wp_enqueue_script( 'bd_customer_pricing', plugins_url('assets/main.js', __FILE__) );

        wp_enqueue_style( 'woocommerce_admin_styles', WC()->plugin_url() . '/assets/css/admin.css', array(), WC_VERSION );
        wp_enqueue_script( 'select2', WC()->plugin_url() . '/assets/js/select2/select2' . $suffix . '.js', array( 'jquery' ), '3.5.4' );
        wp_enqueue_script( 'wc-enhanced-select', WC()->plugin_url() . '/assets/js/admin/wc-enhanced-select' . $suffix . '.js', array( 'jquery', 'select2' ), WC_VERSION );

        add_thickbox();
    }

    public function user_actions($actions, $user_object) {
        $nonce = wp_create_nonce( 'wp_rest' );
        $actions['pricing'] = sprintf("<a href='%s'>%s</a>", add_query_arg(array( 'page' => 'customer-pricing', 'user_id' => $user_object->ID ), admin_url('admin.php')), "Pricing");
        $actions['export'] = sprintf("<a href='%s'>%s</a>", esc_url_raw( get_rest_url() ) . 'bd-customer-pricing/v1/customer/' . $user_object->ID . '/pricing.csv?_wpnonce=' . $nonce, "Export Spreadsheet");
        return $actions;
    }

    public function admin_setup() {
        $page = add_submenu_page(
            'nopage.php',
            'Customer Pricing',
            'Customer Pricing',
            'manage_options',
            'customer-pricing',
            array( $this, 'admin_page' )
        );
        add_action( 'admin_print_styles-' . $page, array($this, 'assets'));
    }

    public function admin_page() {

        if(!isset($_GET['user_id']) || empty($_GET['user_id'])) wp_redirect(admin_url('users.php'));
        $user = get_userdata($_GET['user_id']);
        if(!$user) wp_redirect(admin_url('users.php'));

        $data = BD_Util::getCustomerData($user);
        include 'includes/admin-page.php';

    }
    public function price_modal() {
        include 'includes/price-modal.php';
        exit;
    }

    public function get_variations() {
        echo BD_Util::get_variations($_GET['product_id']);
        exit;
    }

    public function delete_row() {
        global $wpdb;

        $result = $wpdb->delete($this->table_name, array('id' => $_POST['row_id']));
        exit;
    }

    public function get_price($price, $product, $user_id = null) {

        $product_id = $product->get_id();
        $parent = $product->get_parent();
        if(is_a($product, 'WC_Product_Variation')) {
            $product_id = $product->parent->id;
        }

        $user = wp_get_current_user();
        if ($user_id) {
            $user = get_user_by( 'id', $user_id );
        }

        remove_filter('woocommerce_get_price', array( $this, 'get_price' ), 50);
        $data = BD_Util::getCustomerData($user);
        add_filter('woocommerce_get_price', array( $this, 'get_price' ), 50, 2);

        $discount = 0;

        if(isset($data['global'])) {
            $discount = $data['global']->discount;
        }

        $main_categories = array_keys(BD_Util::get_top_level_categories($parent));
        $cats = get_the_terms($parent, 'product_cat');
        $child_categories = array();
        if($cats) {
            $child_categories = array_map(function($item) { return $item->term_id; }, $cats);
        }
        if(!empty($data['category'])) {
            foreach($data['category'] as $row) {
                if(in_array($row->item_id, $child_categories)) {
                    $discount = $row->discount;
                } else if(in_array($row->item_id, $main_categories)) {
                    $discount = $row->discount;
                }
            }
        }

        if(!empty($data['product'])) {
            foreach($data['product'] as $row) {
                if($row->item_id == $product_id) {
                    $discount = $row->discount;
                } else if($row->item_id == $parent || $row->product_id == $parent) {
                    $discount = $row->discount;
                }
            }
        }

        if(is_numeric($price)) return $price * (1 - ($discount / 100));
        return $price;

    }

    public function register_routes() {
        register_rest_route('bd-customer-pricing/v1', '/customer/(?P<id>\d+)/pricing.csv', array(
            'methods' => WP_REST_Server::READABLE,
            'callback' => array($this, 'export_pricing'),
            'permission_callback' => array($this, 'rest_permissions'),
            'args' => array(
                'id' => array(
                    'validate_callback' => function($param, $request, $key) {
                        return is_numeric( $param );
                    }
                ),
            ),
        ));
    }

    public function rest_permissions($request) {
        return current_user_can('edit_users');
    }

    public function export_pricing(WP_REST_Request $request) {

        $user_id = absint($request->get_param('id'));

        $response = array();

        $products = get_posts(array(
            'posts_per_page' => -1,
            'post_type' => 'product',
            'post_status' => 'publish',
            'post_parent' => 0
        ));

        foreach ($products as $product) {
            $children = get_children(array(
                'post_parent' => $product->ID,
                'post_type'   => 'product',
                'numberposts' => -1,
                'post_status' => 'any'
            ));

            $category = get_term(get_product_top_level_category($product->ID), 'product_cat');

            foreach ($children as $child) {
                $wc_product = wc_get_product($child);
                $data = array(
                    'Our Code' => $wc_product->get_sku(),
                    'Category' => html_entity_decode($category->name),
                    'Description' => $product->post_title,
                    'Detail' => '',
                    'List Weekly Hire' => 0,
                    'List Daily Hire' => 0,
                    'List 2 Day Hire Rate' => 0
                );

                $title = str_replace($wc_product->get_sku() . ' - ', '', $wc_product->get_title());
                $data['Detail'] = $title;

                foreach ($wc_product->get_children() as $variation_id) {
                    $variation = wc_get_product($variation_id);
                    $attributes = $variation->get_variation_attributes();
                    $hire_rate = get_term_by('slug', $attributes['attribute_pa_hire-rate'], 'pa_hire-rate')->name;

                    remove_filter('woocommerce_get_price', array( $this, 'get_price' ), 50);
                    $price = $variation->get_price();
                    $data['List ' . $hire_rate] = number_format($this->get_price($price, $variation, $user_id), 2);
                }

                $response[] = $data;
            }
        }

        header("Content-type: text/csv");
        echo BD_Util::str_putcsv($response);
        exit;
    }
}
BD_Customer_Pricing::get_instance();