jQuery(document).ready(function($) {

    $(".bd-wrap").on('click', '.add-row', function(event) {
        event.preventDefault();
        var $table = $(this).parents("section").find("table");
        var $row = $(this).parents("section").find(".blank-row").html();
        $table.find('tbody').append($row);
        $table.find('.no-rows').hide();
        $table.trigger('wc-enhanced-select-init');
    });

    $(".bd-wrap").on('click', 'a.remove', function(event) {
        event.preventDefault();
        var $table = $(this).parents("section").find("table");
        var $row = $(this).parents("tr");
        var id = $row.data('id');
        $row.remove();

        if(id) {
            $.post({
                url: wc_enhanced_select_params.ajax_url,
                data: {
                    action: 'bd_delete_row',
                    row_id: id
                }
            });
        }

        if($table.find('tbody tr:visible').length <= 0) {
            $table.find('.no-rows').show();
        }
    });

    $(".bd-wrap").on('click', 'a.calculate', function(event) {
        event.preventDefault();
        var variation_id = parseInt($(this).parents('tr').find('select.variation-select').val());
        if(variation_id > 0) {
            window.return_row = $(this).parents('tr');
            tb_show('Calculate Price', wc_enhanced_select_params.ajax_url + '?action=bd_calculate_price&variation_id=' + variation_id, false);
        }
    });

    $(document).on('keyup', 'form.price-modal .desired', function() {
        var $form = $("form.price-modal");
        var $target = $(".difference", $form);
        var $source = parseFloat($(".original", $form).val());
        var $desired = parseFloat($(this).val());
        $target.val(100 - (($desired / $source) * 100));
    });

    $(document).on('submit', 'form.price-modal', function(event) {
        event.preventDefault();
        var $target = $(".difference", this);
        if(window.return_row) {
            $(window.return_row).find('.discount').val($target.val());
            window.return_row = undefined;
        }
        tb_remove();
    });



    $(".bd-wrap").on('change', '.wc-product-search', function (event) {
        var product_id = $(event.currentTarget).val();
        var $row = $(this).parents("tr");
        var $select = $row.find('select.variation-select');
        $.get({
            url: wc_enhanced_select_params.ajax_url,
            data: {
                action: 'bd_get_variations',
                product_id: product_id
            },
            success: function(response) {
                $select.html(response);
                var select2Instance = $select.data('select2');
                var resetOptions = select2Instance.options.options;
                $select.select2('destroy').select2(resetOptions);
            }
        });
    });

});